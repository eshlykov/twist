#pragma once

// https://en.wikipedia.org/wiki/Futex

#if LINUX

namespace twist {
namespace thread {

// Simple wrappers around futex syscall on Linux

int FutexWait(unsigned int* addr, int expected);

int FutexWake(unsigned int* addr, int count);

}  // namespace thread
}  // namespace twist

#endif
