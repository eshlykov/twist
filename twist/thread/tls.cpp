#include <twist/thread/tls.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/sequencer.hpp>

#include <atomic>
#include <cassert>
#include <cstring>
#include <cstdint>

namespace twist {
namespace thread {
namespace tls {

//////////////////////////////////////////////////////////////////////

class Storage {
  static const size_t kCapacity = 1024;

 public:
  Storage() {
    memset(slots_, 0, sizeof(void*) * kCapacity);
  }

  void* AccessSlot(size_t key) {
    return (void*)(slots_ + key);
  }

  // append-only
  Key AcquireKey() {
    size_t key = sequencer.Next();
    assert(key < kCapacity);
    return key;
  }

  void ReleaseKey(Key key) {
    UNUSED(key);
    // not implemented
  }

 private:
  std::uintptr_t slots_[kCapacity];
  static Sequencer sequencer;
};

Sequencer Storage::sequencer;

//////////////////////////////////////////////////////////////////////

static thread_local Storage tls;

//////////////////////////////////////////////////////////////////////

Key AcquireKey() {
  return tls.AcquireKey();
}

void ReleaseKey(Key key) {
  tls.ReleaseKey(key);
}

void* AccessSlot(Key key) {
  return tls.AccessSlot(key);
}

}  // namespace tls
}  // namespace thread
}  // namespace twist
