#pragma once

#include <twist/support/string_builder.hpp>
#include <twist/support/source_location.hpp>

// Simple asynchronous logging

namespace twist {

//////////////////////////////////////////////////////////////////////

enum class LogLevel : int {
  Trace = 1,
  Debug = 2,
  Info = 3,
  Warning = 4,
  Critical = 5
};

//////////////////////////////////////////////////////////////////////

bool LevelAccepted(LogLevel level);

void LogMessage(SourceLocation where, std::string message);

void FlushPendingLogMessages();

//////////////////////////////////////////////////////////////////////

void LogMessageSimple(std::string message);

//////////////////////////////////////////////////////////////////////

#define LOG_IMPL(level, expr)                           \
  if (twist::LevelAccepted(level)) {                    \
    twist::LogMessage(HERE(), StringBuilder() << expr); \
  }

#define LOG_DEBUG(expr) LOG_IMPL(twist::LogLevel::Debug, expr)
#define LOG_TRACE(expr) LOG_IMPL(twist::LogLevel::Trace, expr)
#define LOG_INFO(expr) LOG_IMPL(twist::LogLevel::Info, expr)

// Usage: LOG_SIMPLE("Key " << key << " not found")
#define LOG_SIMPLE(expr) twist::LogMessageSimple(StringBuilder() << expr);

}  // namespace twist
