#pragma once

#include <cstddef>

// Thread-local storage

namespace twist {
namespace th {

using Key = size_t;

Key AcquireKey();
void ReleaseKey(Key key);
void* AccessSlot(Key key);

}  // namespace th
}  // namespace twist
