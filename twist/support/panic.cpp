#include <twist/support/panic.hpp>

#include <twist/support/locking.hpp>

namespace detail {

void Panic(const std::string& error) {
  static std::mutex mutex;

  {
    auto guard = twist::LockUnique(mutex);
    std::cerr << error << std::endl;
  }

  std::abort();
}

}  // namespace detail
