#include <twist/support/sync_output.hpp>

namespace twist {

SyncOutput& SyncCout() {
  static SyncOutput cout(std::cout);
  return cout;
}

SyncOutput& SyncCerr() {
  static SyncOutput cerr(std::cerr);
  return cerr;
}

}  // namespace twist
