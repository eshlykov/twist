#pragma once

#include <twist/fault/adversary.hpp>

namespace twist {
namespace fault {

inline void InjectFault() {
  GetAdversary()->Fault();
}

}  // namespace fault
}  // namespace twist

// Voluntarily inject fault (yield/sleep/park) into current thread

#if defined(TWIST_FAULTY)

#define TWIST_VOLUNTARY_FAULT() twist::fault::InjectFault();

#else

#define TWIST_VOLUNTARY_FAULT()

#endif
