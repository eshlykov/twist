#include <twist/fault/adversary.hpp>

#include <twist/threading/stdlike.hpp>

#include <twist/support/assert.hpp>
#include <twist/support/intrusive_list.hpp>
#include <twist/support/locking.hpp>
#include <twist/support/one_shot_event.hpp>
#include <twist/support/panic.hpp>
#include <twist/support/random.hpp>
#include <twist/support/singleton.hpp>
#include <twist/support/sync_output.hpp>
#include <twist/logging/logging.hpp>

#include <atomic>
#include <cstdint>
#include <iostream>
#include <thread>

namespace twist {
namespace fault {

/////////////////////////////////////////////////////////////////////

class Yielder {
 public:
  Yielder(size_t freq) : freq_(freq) {
  }

  void Reset() {
    yield_count_ = 0;
  }

  void MaybeYield() {
    if (left_.fetch_sub(1, std::memory_order_relaxed) == 1) {
      left_.store(twist::RandomUInteger(1, freq_));
      twist::th::this_thread::yield();
      yield_count_.fetch_add(1, std::memory_order_relaxed);
    }
  }

  size_t GetYieldCount() const {
    return yield_count_.load(std::memory_order_relaxed);
  }

 private:
  size_t freq_;
  std::atomic<size_t> left_{1};

  // statistics
  std::atomic<size_t> yield_count_{0};
};

/////////////////////////////////////////////////////////////////////

class YieldAdversary : public IAdversary {
 public:
  YieldAdversary(size_t yield_freq) : yielder_(yield_freq) {
  }

  // per-test methods

  void Reset() override {
    yielder_.Reset();
  }

  void PrintReport() override {
    SyncCout() << "Context switches injected: " << yielder_.GetYieldCount()
               << std::endl;
  }

  // per-thread methods

  void Enter() override {
    // do nothing
  }

  void Fault() override {
    yielder_.MaybeYield();
  }

  void ReportProgress() override {
    // ignore lock-free algorithms
  }

  void Exit() override {
    // do nothing
  }

 private:
  Yielder yielder_;
};

/////////////////////////////////////////////////////////////////////

namespace {

// not thread safe!
class ThreadWaitQueue {
 public:
  struct WaitNode : public IntrusiveListNode<WaitNode>,
                    public twist::OneShotEvent {
    void Wake() {
      Set();
    }
  };

  using WaitQueue = IntrusiveList<WaitNode>;

 public:
  // suspend current thread
  void Enter(WaitNode* this_thread_node) {
    wait_queue_.PushBack(this_thread_node);
  }

  void ResumeRandomThread() {
    if (!wait_queue_.IsEmpty()) {
      auto* wait_node = UnlinkRandomItem(wait_queue_);
      wait_node->Wake();
    }
  }

 private:
  WaitQueue wait_queue_;
};

class RandomTicker {
 public:
  RandomTicker(size_t tick_limit = 10) : tick_limit_(tick_limit) {
    Reset();
  }

  // wait-free
  bool Tick() {
    if (ticks_left_.fetch_sub(1) == 1) {
      // last tick
      Reset();
      return true;
    }
    return false;
  }

  void Reset() {
    ticks_left_.exchange(twist::RandomUInteger(1, tick_limit_));
  }

 private:
  size_t tick_limit_;
  std::atomic<int> ticks_left_{0};
};

}  // namespace

class LockFreeAdversary : public IAdversary {
 public:
  LockFreeAdversary(size_t suspend_freq, size_t suspended_thread_limit,
                    size_t yield_freq)
      : suspended_thread_limit_(suspended_thread_limit),
        suspend_(suspend_freq),
        resume_(suspend_freq / 2),
        yielder_(yield_freq) {
  }

  void Reset() override {
    suspend_.Reset();
    resume_.Reset();
    yielder_.Reset();
  }

  void PrintReport() override {
    VERIFY(suspended_thread_count_ == 0, "some threads still suspended");
  }

  // per-thread methods

  void Enter() override {
    // todo: check thread ids
    ++thread_count_;
  }

  void Fault() override {
    size_t inject_count = inject_count_.fetch_add(1, std::memory_order_relaxed);

    if (suspend_.Tick()) {
      SuspendThisThread(inject_count);
    } else {
      yielder_.MaybeYield();
    }
  }

  void ReportProgress() override {
    // wait-free on fast path, locking on slow path
    if (resume_.Tick()) {
      if (suspended_thread_count_ > 0) {
        ResumeThread();
      }
    }
  }

  void Exit() override {
    // todo: check thread ids

    size_t threads_left = thread_count_--;
    LOG_SIMPLE("Thread exit (left: " << threads_left << ")");

    // we need at least one running thread
    ResumeThread();
  }

 private:
  void ResumeThread() {
    auto lock = twist::LockUnique(mutex_);
    suspended_threads_.ResumeRandomThread();
  }

  void SuspendThisThread(size_t start_inject_count) {
    ThreadWaitQueue::WaitNode this_thread_node;

    {
      auto lock = twist::LockUnique(mutex_);

      if (suspended_thread_count_ >= suspended_thread_limit_) {
        // too many suspended threads
        return;
      }

      if (suspended_thread_count_ + 1 >= thread_count_) {
        // at least one thread should run
        return;
      }

      suspended_threads_.Enter(&this_thread_node);
      ++suspended_thread_count_;
      LOG_SIMPLE("Suspend thread by adversary (suspended: "
                 << suspended_thread_count_ << ")");
    }

    this_thread_node.Await();
    --suspended_thread_count_;
    size_t suspend_time = inject_count_ - start_inject_count;
    LOG_SIMPLE("Thread resumed by adversary, suspend 'time': "
               << suspend_time << " (suspended: " << suspended_thread_count_
               << ")");
  }

 private:
  std::atomic<size_t> inject_count_{0};

  std::mutex mutex_;

  std::atomic<size_t> thread_count_;

  size_t suspended_thread_limit_;
  RandomTicker suspend_;
  ThreadWaitQueue suspended_threads_;
  std::atomic<size_t> suspended_thread_count_{0};

  RandomTicker resume_;

  Yielder yielder_;
};

/////////////////////////////////////////////////////////////////////

class Holder {
 public:
  Holder() : adversary_(CreateDefaultAdversary()) {
  }

  IAdversary* Get() {
    return adversary_.get();
  }

  void Set(IAdversaryPtr adversary) {
    adversary_ = std::move(adversary);
  }

 private:
  static IAdversaryPtr CreateDefaultAdversary() {
    return std::make_shared<YieldAdversary>(10);
  }

 private:
  IAdversaryPtr adversary_;
};

IAdversary* GetAdversary() {
  return Singleton<Holder>()->Get();
}

void SetAdversary(IAdversaryPtr adversary) {
  Singleton<Holder>()->Set(std::move(adversary));
}

}  // namespace fault
}  // namespace twist
