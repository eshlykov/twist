#pragma once

#include <twist/fault/mutex.hpp>

#include <memory>
#include <mutex>

namespace twist {
namespace fault {

class FaultyConditionVariable {
 public:
  using Lock = std::unique_lock<FaultyMutex>;

  FaultyConditionVariable();
  ~FaultyConditionVariable();

  void wait(Lock& lock);  // NOLINT

  template <class Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one();  // NOLINT
  void notify_all();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fault
}  // namespace twist
