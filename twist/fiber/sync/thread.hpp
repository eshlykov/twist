#pragma once

#include <twist/fiber/core/api.hpp>

#include <memory>

namespace twist {
namespace fiber {

class ThreadLike {
 public:
  using id = FiberId;  // NOLINT

 public:
  ThreadLike(FiberRoutine routine);
  ~ThreadLike();

  ThreadLike(ThreadLike&& that);
  ThreadLike& operator=(ThreadLike&& that);

  void Join();

  // std::thread interface

  void join() {  // NOLINT
    Join();
  }

  bool joinable() const {  // NOLINT
    return true;
  }

  void detach() {  // NOLINT
    throw std::runtime_error("detach not implemented");
  }

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fiber
}  // namespace twist
