#include <twist/fiber/core/impl.hpp>

#include <twist/fiber/core/asan.hpp>

#include <twist/logging/logging.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/panic.hpp>
#include <twist/support/random.hpp>
#include <twist/support/sequencer.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

static FiberId GenerateId() {
  static Sequencer sequencer;
  return sequencer.Next();
}

Fiber* Fiber::Create(FiberRoutine routine) {
  auto* fiber = new Fiber();

  fiber->stack_ = FiberStack::Allocate();
  fiber->id_ = GenerateId();
  fiber->routine_ = std::move(routine);
  fiber->state_ = FiberState::Starting;

  SetupTrampoline(fiber);

  return fiber;
}

//////////////////////////////////////////////////////////////////////

static void FiberTrampoline() {
  AsanAfterSwitch();

  Fiber* self = GetCurrentFiber();

  self->SetState(FiberState::Running);

  auto routine = self->UserRoutine();
  try {
    routine();
  } catch (...) {
    PANIC("Uncaught exception in fiber " << self->Id());
  }

  GetCurrentScheduler()->Terminate();  // never returns

  UNREACHABLE();
}

void Fiber::SetupTrampoline(Fiber* fiber) {
  fiber->context_.Setup(
      /*stack=*/fiber->stack_.AsMemSpan(),
      /*trampoline=*/FiberTrampoline);
}

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current_fiber;

Fiber* GetCurrentFiber() {
  return current_fiber;
}

static inline Fiber* GetAndResetCurrentFiber() {
  auto* f = current_fiber;
  current_fiber = nullptr;
  return f;
}

static inline void SetCurrentFiber(Fiber* f) {
  current_fiber = f;
}

//////////////////////////////////////////////////////////////////////

static thread_local Scheduler* current_scheduler;

Scheduler* GetCurrentScheduler() {
  VERIFY(current_scheduler, "not in fiber context");
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    VERIFY(!current_scheduler, "cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler() : stack_(thread::Stack::ThisThread()) {
  context_.Setup(stack_.AsMemSpan());
}

// Operations invoked by running fibers

void Scheduler::SwitchToScheduler() {
  Fiber* caller = GetAndResetCurrentFiber();
  caller->Context().SwitchTo(context_);
}

void Scheduler::Spawn(FiberRoutine routine) {
  auto* created = CreateFiber(routine);
  Schedule(created);
}

void Scheduler::Yield() {
  if (preempt_disabled_) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToScheduler();
}

void Scheduler::SleepFor(Duration duration) {
  // Intentionally ineffective implementation
  // Support for sleep in scheduler left as homework

  Timer timer;
  do {
    Yield();
  } while (timer.Elapsed() < duration);
}

void Scheduler::Suspend() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Suspended);
  SwitchToScheduler();
}

void Scheduler::Resume(Fiber* that) {
  that->SetState(FiberState::Runnable);
  Schedule(that);
}

void Scheduler::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToScheduler();
}

// Scheduling

void Scheduler::Run(FiberRoutine main, size_t fuel) {
  SchedulerScope scope(this);
  SpawnMainFiber(main);
  RunLoop(fuel);
  CheckDeadlock();
}

void Scheduler::SpawnMainFiber(FiberRoutine main) {
  auto* fiber = CreateFiber(main);
  Schedule(fiber);
}

void Scheduler::RunLoop(size_t fuel) {
  while (!ready_.IsEmpty() && fuel-- > 0) {
    Fiber* next = PickReadyFiber();
    SwitchTo(next);
    Reschedule(next);
  }
}

void Scheduler::CheckDeadlock() {
  if (ready_.IsEmpty() && alive_count_ > 0) {
    throw DeadlockDetected();
  }
}

Fiber* Scheduler::PickReadyFiber() {
#if defined(TWIST_FAULTY)
  return UnlinkRandomItem(ready_);
#else
  return ready_.PopFront();
#endif
}

void Scheduler::SwitchTo(Fiber* fiber) {
  ++switch_count_;
  SetCurrentFiber(fiber);
  fiber->SetState(FiberState::Running);
  // scheduler context_ -> fiber->context_
  context_.SwitchTo(fiber->Context());
}

void Scheduler::Reschedule(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Suspended:  // From Suspend
      // Do nothing
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  ready_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  ++alive_count_;
  return Fiber::Create(routine);
}

void Scheduler::Destroy(Fiber* fiber) {
  --alive_count_;
  delete fiber;
}

}  // namespace fiber
}  // namespace twist
