#include <twist/fiber/core/fls.hpp>

#include <twist/fiber/core/impl.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/sequencer.hpp>

#include <atomic>

namespace twist {
namespace fiber {
namespace fls {

size_t AcquireKey() {
  static Sequencer sequencer;
  return sequencer.Next();
}

void ReleaseKey(Key key) {
  UNUSED(key);
  // not implemented
}

void* AccessSlot(Key key) {
  Fiber* self = GetCurrentFiber();
  std::uintptr_t* fls = self->LocalStorage();
  return (void*)(fls + key);
}

}  // namespace fls
}  // namespace fiber
}  // namespace twist
