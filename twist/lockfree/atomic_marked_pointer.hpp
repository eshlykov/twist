#pragma once

#include <twist/stdlike/atomic.hpp>

#include <atomic>
#include <cassert>
#include <cstddef>

namespace twist {
namespace lockfree {

////////////////////////////////////////////////////////////////////////////////

// Pointer + mark bit packed into a single machine word
// Intended for conflict materialization in lock-free list-based algorithms

template <typename T>
class AtomicMarkedPointer {
  using PackedPointer = uintptr_t;

 public:
  struct MarkedPointer {
    MarkedPointer(T* ptr, bool marked) : ptr_(ptr), marked_(marked) {
    }

    T* operator->() const {
      return ptr_;
    }

    T& operator*() const {
      return *ptr_;
    }

    explicit operator bool() const {
      return ptr_ != nullptr;
    }

    bool operator==(const MarkedPointer& that) const {
      return (ptr_ == that.ptr_) && (marked_ == that.marked_);
    }

    bool IsMarked() const {
      return marked_;
    }

    T* ptr_;
    bool marked_;
  };

 public:
  explicit AtomicMarkedPointer(T* ptr = nullptr)
      : packed_ptr_{Pack({ptr, false})} {
  }

  MarkedPointer Load() const {
    return Unpack(packed_ptr_.load());
  }

  T* LoadPointer() const {
    const auto marked_ptr = Load();
    return marked_ptr.ptr_;
  }

  // Usage: atomic_marked_ptr.Store({raw_ptr, false})

  void Store(MarkedPointer marked_ptr) {
    packed_ptr_.store(Pack(marked_ptr));
  }

  void Store(T* ptr) {
    Store(MarkedPointer{ptr, false});
  }

  AtomicMarkedPointer& operator=(T* ptr) {
    Store(MarkedPointer{ptr, false});
    return *this;
  }

  bool TryMark(T* ptr) {
    return CompareAndSet({ptr, false}, {ptr, true});
  }

  bool IsMarked() const {
    return packed_ptr_.load() & 1u;

    // const auto curr_ptr = Load();
    // return curr_ptr.marked_;
  }

  // Usage: atomic_marked_ptr.CompareAndSet({expected_ptr, false}, {desired_ptr,
  // false}) Note: expected passed by value!

  bool CompareAndSet(MarkedPointer expected, MarkedPointer desired) {
    auto expected_packed = Pack(expected);
    return packed_ptr_.compare_exchange_strong(expected_packed, Pack(desired));
  }

 private:
  static PackedPointer Pack(MarkedPointer marked_ptr) {
    return reinterpret_cast<uintptr_t>(marked_ptr.ptr_) ^
           (marked_ptr.marked_ ? 1 : 0);
  }

  static MarkedPointer Unpack(PackedPointer packed_ptr) {
    uintptr_t marked_bit = packed_ptr & 1u;
    T* raw_ptr = reinterpret_cast<T*>(packed_ptr ^ marked_bit);
    return {raw_ptr, marked_bit != 0};
  }

 private:
  twist::atomic<PackedPointer> packed_ptr_;
};

}  // namespace lockfree
}  // namespace twist
