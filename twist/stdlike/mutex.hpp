#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/mutex.hpp>

namespace twist {
using mutex = fault::FaultyMutex;  // NOLINT
}  // namespace twist

#else

#include <mutex>

namespace twist {
using std::mutex;
}  // namespace twist

#endif
