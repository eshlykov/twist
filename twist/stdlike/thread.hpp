#pragma once

#include <twist/threading/stdlike.hpp>

namespace twist {

// https://en.cppreference.com/w/cpp/thread/thread
using thread = th::thread;  // NOLINT

namespace this_thread {

// https://en.cppreference.com/w/cpp/thread/yield
auto& yield = th::this_thread::yield;  // NOLINT

// https://en.cppreference.com/w/cpp/thread/sleep_for
auto& sleep_for = th::this_thread::sleep_for;  // NOLINT

}  // namespace this_thread

}  // namespace twist
