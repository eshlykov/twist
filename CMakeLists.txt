cmake_minimum_required(VERSION 3.9)
project(twistlib)

include(cmake/BuildTypes.cmake)
include(cmake/ClangFormat.cmake)
include(cmake/ClangTidy.cmake)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory(third_party)

include(cmake/Platform.cmake)
include(cmake/Processor.cmake)

add_compile_options(-Wall -Wextra -Wpedantic -g -fno-omit-frame-pointer)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")

add_subdirectory(twist)

option(TWIST_TESTS "Enable twist library tests" OFF)
option(TWIST_BENCHMARKS "Enable twist library benchmarks" OFF)

if(${TWIST_TESTS})
    add_subdirectory(tests)
endif()

if(${TWIST_BENCHMARKS})
    add_subdirectory(benchmarks)
endif()

