#include <twist/test_utils/executor.hpp>

#include <twist/support/sleep.hpp>
#include <twist/test_framework/test_framework.hpp>

TEST_SUITE(ScopedExecutor) {
  SIMPLE_TEST(OneTask) {
    twist::ScopedExecutor executor;

    executor.Submit([]() { twist::SleepMillis(500); });
  }

  SIMPLE_TEST(ManyTasks) {
    twist::ScopedExecutor executor;

    for (size_t i = 0; i < 10; ++i) {
      executor.Submit([]() { twist::SleepRandomMillis(100, 200); });
    }
  }
}
