#include <twist/lockfree/atomic_marked_pointer.hpp>

#include <twist/test_framework/test_framework.hpp>

TEST_SUITE(AtomicMarkedPointer) {
  SIMPLE_TEST(DefaultCtor) {
    twist::lockfree::AtomicMarkedPointer<int> marked_ptr;
    ASSERT_EQ(marked_ptr.LoadPointer(), nullptr);
    ASSERT_FALSE(marked_ptr.IsMarked());
  }

  SIMPLE_TEST(Ctor) {
    int* ptr = new int{17};
    twist::lockfree::AtomicMarkedPointer<int> marked_ptr(ptr);
    ASSERT_EQ(marked_ptr.LoadPointer(), ptr);
    delete ptr;
  }

  SIMPLE_TEST(StampedPtrAsRawPtr) {
    twist::lockfree::AtomicMarkedPointer<int> atomic_marked_ptr(new int{42});
    auto marked_ptr = atomic_marked_ptr.Load();
    ASSERT_TRUE(marked_ptr);
    ASSERT_EQ(*marked_ptr, 42);
  }

  SIMPLE_TEST(StoreLoad) {
    int* ptr = new int{42};
    twist::lockfree::AtomicMarkedPointer<int> marked_ptr;

    marked_ptr.Store(ptr);
    ASSERT_EQ(marked_ptr.LoadPointer(), ptr);
    ASSERT_FALSE(marked_ptr.IsMarked());

    delete ptr;
    ptr = new int{43};

    marked_ptr.Store({ptr, true});
    ASSERT_EQ(marked_ptr.LoadPointer(), ptr);
    ASSERT_TRUE(marked_ptr.IsMarked());

    delete ptr;
  }

  SIMPLE_TEST(Mark) {
    int* ptr = new int{42};
    twist::lockfree::AtomicMarkedPointer<int> marked_ptr;

    marked_ptr.Store(ptr);
    ASSERT_FALSE(marked_ptr.IsMarked());

    ASSERT_FALSE(marked_ptr.TryMark(nullptr)); // expected value does not match
    ASSERT_FALSE(marked_ptr.IsMarked());

    ASSERT_TRUE(marked_ptr.TryMark(ptr));
    ASSERT_TRUE(marked_ptr.IsMarked());
  }

  SIMPLE_TEST(CompareAndSet) {
    int* ptr = new int{42};

    twist::lockfree::AtomicMarkedPointer<int> marked_ptr;
    ASSERT_TRUE(marked_ptr.CompareAndSet({nullptr, false}, {ptr, false}));
    ASSERT_FALSE(marked_ptr.CompareAndSet({ptr, true}, {nullptr, false}));
    ASSERT_TRUE(marked_ptr.CompareAndSet({ptr, false}, {ptr, true}));
    ASSERT_TRUE(marked_ptr.IsMarked());

    delete ptr;
  }
}
